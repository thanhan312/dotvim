#!/bin/bash

# Clone
git clone git@bitbucket.org:haild/dotvim.git ~/.vim

# Create symlinks
ln -s ~/.vim/vimrc ~/.vimrc

# Install Vundle submodule
cd ~/.vim
git submodule init
git submodule update

# Install Vim plugins
vim +PluginInstall +qa
