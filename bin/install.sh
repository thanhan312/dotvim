#!/bin/bash
git clone git@bitbucket.org:haild/dotvim.git ~/.vim
ln -s ~/.vim/vimrc ~/.vimrc

cd ~/.vim
git submodule init
git submodule update
git submodule foreach git checkout master

cd ~/.vim/bundle/jedi-vim/
git submodule init
git submodule update
git submodule foreach git checkout master
