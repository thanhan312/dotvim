" vim-plug https://github.com/junegunn/vim-plug {{{
call plug#begin()
function! DoRemote(arg)
  UpdateRemotePlugins
endfunction
Plug 'Shougo/deoplete.nvim', { 'do': function('DoRemote') }
Plug 'davidhalter/jedi-vim'
Plug 'zchee/deoplete-jedi'
Plug 'will133/vim-dirdiff'
Plug 'altercation/vim-colors-solarized'
Plug 'bling/vim-airline'
Plug 'dag/vim-fish'
Plug 'dkprice/vim-easygrep'
Plug 'ervandew/supertab'
Plug 'crusoexia/vim-monokai'
Plug 'hynek/vim-python-pep8-indent'
Plug 'kien/ctrlp.vim'
Plug 'nvie/vim-flake8'
Plug 'othree/html5.vim'
Plug 'chase/vim-ansible-yaml'
Plug 'scrooloose/nerdtree'
Plug 'scrooloose/syntastic'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-sensible'
Plug 'heavenshell/vim-pydocstring'
Plug 'jlanzarotta/bufexplorer'
Plug 'chrisbra/vim-diff-enhanced'
Plug 'janko-m/vim-test'
Plug 'ekalinin/Dockerfile.vim'
Plug 'mfukar/robotframework-vim'
Plug 'airblade/vim-gitgutter'
call plug#end()
" }}}

" Performance
set shell=bash
set noswapfile
if !has('nvim')
    set ttyfast
endif

set lazyredraw
set modeline!

" Editing
set shiftwidth=4
set tabstop=4
set softtabstop=4
set smarttab
set expandtab

" UI
"set switchbuf+=usetab,newtab
set relativenumber
set number
set laststatus=2

set background=dark
if has('nvim')
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
    colorscheme solarized
else
    if has("gui_running")
        colorscheme solarized
    else
        set t_Co=256
        set t_ZH=[3m
        set t_ZR=[23m
        let g:monokai_italic = 1
        colorscheme monokai
    endif
endif

" Custom filetype configuration
au BufNewFile,BufRead COMMIT_EDITMSG setlocal spell
au BufNewFile,BufRead .arc* setlocal ft=json
au BufNewFile,BufRead Vagrantfile* setlocal ft=ruby
au BufNewFile,BufRead vim* setlocal foldmethod=marker
au BufNewFile,BufRead *.yml setlocal shiftwidth=2 tabstop=2 softtabstop=2
" Press K for help when editing vim files
au FileType vim setlocal keywordprg=:help

" Custom key mapping
" NERDTree
map <F9> :NERDTreeToggle<CR>

" Write with sudo when vim started with unprevilege user
cmap w!! w !sudo tee > /dev/null %

" Tab
nnoremap <Space> :bnext<CR>
set hidden

let g:EasyGrepRecursive=1
let g:EasyGrepMode=2
let g:EasyGrepJumpToMatch=0

let g:airline#extensions#tabline#enabled=1
let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#fnamemod = ':s?.virtualenvs/.*/lib/python2.7/site-packages?venv?:~:.'

let g:jedi#use_tabs_not_buffers = 0
let g:jedi#show_call_signatures = "2"
let g:jedi#completions_enabled = 0

let g:deoplete#enable_at_startup = 1

set noshowmode

set mouse=nv
set clipboard=unnamed

" let g:pymode_rope_completion=0  " Use Jedi completion instead of Rope
" let g:syntastic_python_checkers = ['pylint']

" LaTeX-suite
" let g:tex_flavor='latex'
" let g:Tex_DefaultTargetFormat='pdf'
" let g:Tex_Folding=0
" let g:Tex_MultipleCompileFormats='dvi,pdf'
" set grepprg=grep\ -nH\ $*
